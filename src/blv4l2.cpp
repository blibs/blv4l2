#include <string.h>
#include <fcntl.h>
#include "blc_image.h"
#include "blv4l2.h"
#include <arpa/inet.h> //htonl
#include <sys/ioctl.h>
#include <unistd.h>
#include <jpeglib.h>

#include <sys/mman.h>
#include <errno.h>
#include <limits.h>

#define V4L_REQUEST(arg, request_value) request(arg, request_value, #request_value)
#define V4L_ASSERT(arg, request) if (!((arg) & (request))) EXIT_ON_ERROR("Device %s not %s", this->name, #request);

enum {
   READ_WRITE_MODE = 0, MMAP_MODE
};

enum { STOP = 0,  RUN};

void blv4l2_device::request(void *arg, int request, char const *request_name)
{
   while (ioctl(fd, request, arg) == -1)
   {
      if (errno == EINTR) continue;
      else EXIT_ON_SYSTEM_ERROR("v4l2 error with request %s on device %s", request_name, name);
   }
}

void blv4l2_device::init(const char *name)
{
   int bytes_per_pixel;
   struct v4l2_format format;
   struct v4l2_capability cap;
   this->name=name;

   SYSTEM_ERROR_CHECK(fd = open(name, O_RDWR /* required */, 0), -1, "Fail to open %s", name);

   V4L_REQUEST(&cap, VIDIOC_QUERYCAP);
   V4L_ASSERT(cap.capabilities, V4L2_CAP_VIDEO_CAPTURE);

   if (cap.capabilities & V4L2_CAP_READWRITE)   mode = READ_WRITE_MODE;
   else if (cap.capabilities & V4L2_CAP_STREAMING){
      mode = MMAP_MODE;
      //     init_mmap();
   }
   else EXIT_ON_ERROR("Unknown capability in '%ld'", cap.capabilities );


   CLEAR(format);
   format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   V4L_REQUEST(&format, VIDIOC_G_FMT);
   image.type='UIN8';
   image.format=htonl(format.fmt.pix.pixelformat);
   bytes_per_pixel=blc_image_get_bytes_per_pixel(&image);

   switch (bytes_per_pixel){
   case -1:
      image.add_dim(format.fmt.pix.sizeimage);
      break;
   case 1:
      image.add_dim(format.fmt.pix.width);
      image.add_dim(format.fmt.pix.height);
      break;
   default:
      image.add_dim(bytes_per_pixel);
      image.add_dim(format.fmt.pix.width);
      image.add_dim(format.fmt.pix.height);
    }

}

void **blv4l2_device::init_mmap(){
   void **buffers;
   unsigned int buffers_nb;
   enum v4l2_buf_type type;
   struct v4l2_requestbuffers v4l2_request_buffers;

   CLEAR(v4l2_request_buffers);

   v4l2_request_buffers.count = 2;
   v4l2_request_buffers.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   v4l2_request_buffers.memory = V4L2_MEMORY_MMAP;

   V4L_REQUEST(&v4l2_request_buffers, VIDIOC_REQBUFS);

   if (v4l2_request_buffers.count < 2) EXIT_ON_ERROR("Insufficient buffer memory on %s\n", name);
   buffers_nb = v4l2_request_buffers.count;

   CLEAR(v4l2_buffer);
   v4l2_buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   v4l2_buffer.memory = V4L2_MEMORY_MMAP;

   buffers = MANY_ALLOCATIONS(buffers_nb, void*);
   CLEAR(v4l2_buffer);
   v4l2_buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   v4l2_buffer.memory = V4L2_MEMORY_MMAP;

   FOR(v4l2_buffer.index, buffers_nb){
      V4L_REQUEST(&v4l2_buffer, VIDIOC_QUERYBUF);
      SYSTEM_ERROR_CHECK(buffers[v4l2_buffer.index] = mmap(NULL , v4l2_buffer.length, PROT_READ | PROT_WRITE /* required */, MAP_SHARED /* recommended */, fd, v4l2_buffer.m.offset), MAP_FAILED, NULL);
      V4L_REQUEST(&v4l2_buffer, VIDIOC_QBUF);
   }

   type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
   V4L_REQUEST(&type, VIDIOC_STREAMON);

   v4l2_buffer.index = 0;
   return buffers;
}

void blv4l2_device::get_read_write(void *image_data, size_t size)
{
   fd_set fds;
   int result;
   struct timeval tv =
         { 1, 0 }; //timeout 1s

   FD_ZERO(&fds);
   FD_SET(fd, &fds);

   do{
      result = select(fd + 1, &fds, NULL, NULL, &tv);
      if ((result == -1) && (errno != EINTR)) EXIT_ON_SYSTEM_ERROR("Select on device %s", name); // EINTR means that the call has been interupted
      else if (result == 0) EXIT_ON_ERROR("Timeout device %s", name);
   } while (result <= 0);

   result = read(fd, image_data, size);
   if (result == -1) EXIT_ON_SYSTEM_ERROR("device %s", name);
}

void blv4l2_device::run(int (*callback)(blc_array *image, void*), void *user_data){
   int run_again;
   void **buffers=NULL;

   switch (mode)
   {
   case READ_WRITE_MODE:
      image.allocate();
      do
      {
         get_read_write(image.data, image.size);
      }
      while(callback(&image, user_data));

      break;
   case MMAP_MODE:
      buffers=init_mmap();
      do
      {
         V4L_REQUEST(&v4l2_buffer, VIDIOC_DQBUF); //wait the new data
         image.data=buffers[v4l2_buffer.index];
         run_again=callback(&image, user_data);
         V4L_REQUEST(&v4l2_buffer, VIDIOC_QBUF); //Give unused memory pointer
      }
      while(run_again);
      free(buffers);
      break;
   default:
      EXIT_ON_ERROR("Mode %d is not managed.", mode);
   }
}

START_EXTERN_C

void run_capture(char const* device_name, int (*callback)(blc_array *image, void*), void *user_data){
   blv4l2_device device;

   device.init(device_name);
   device.run(callback, user_data);
}

END_EXTERN_C
