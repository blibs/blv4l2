#include "blc_image.h"
#include "blv4l2.h"



blc_array converted_image;
int initialized=0;
int width, height;


static int format_can_be_displayed(uint32_t format){

   switch (format){
      case 'yuv2':return 1;
      break;
      default:;
   }
   return 0;
}


//This is called each time there is a new image available
static int capture_callback(blc_array *image, void *user_data){
    uint32_t format_str;
    (void)user_data; //Avoid unused warning
    
    if (!initialized) //This is executed only the first time
    {

       if (image->format=='MJPG'){
          blc_image_jpeg_init(&converted_image, image);
          width=converted_image.dims[converted_image.dims_nb-2].length;
          height=converted_image.dims[converted_image.dims_nb-1].length;

       }
       else{
          width=image->dims[image->dims_nb-2].length;
          height=image->dims[image->dims_nb-1].length;
       }
        fprintf(stderr, "%dx%d, format:%.4s\n", width, height, UINT32_TO_STRING(format_str, converted_image.format));
        eprintf_escape_command("s");
        initialized=1;
        if (format_can_be_displayed(image->format)) blc_fprint_color_scale(stderr);
    }
    
    switch (image->format){
       case 'MJPG':case 'JPEG':
          blc_image_convert(&converted_image, image);
          if (initialized==2) {
                eprintf_escape_command(MOVE_BEGIN_PREVIOUS_N_LINES, 31);
          }
          else initialized=2;
                     //This function will be simplified in the future
                     fprint_3Dmatrix(stderr, &converted_image.mem, 0, 3, 1, width/32*3, 32, height/32*width*3, 32, 1);
                     break;
          break;
        case 'yuv2':
        //    if (initialized==2) {
      //          blc_eprint_cursor_up(32);//The first time we do not need to go up
               // @TODO This is a macro with bug and need to be in { }
        //    }
          //  else initialized=2;
            if (initialized==2) {
                eprintf_escape_command(MOVE_BEGIN_PREVIOUS_N_LINES, 31);
            }
            else initialized=2;
            
           
            //This function will be simplified in the future
            fprint_3Dmatrix(stderr, &image->mem, 1, 2, 1, width/32*2, 32, height/32*width*2, 32, 1);
            break;
        default:
            break;
    }
    
    //You can use the data in image->chars as you want
    //You may save it as png. ( Require blc_image )
    //You may copy it in a blc_channel. To display the image with f_view_channel (Require blc_channel)
    return 1;
}

int main(int argc, char**argv){
    
   char const *device_name;

   if (argc==2) device_name=argv[1];
   else device_name="/dev/video0";

    fprintf(stderr, "Test blv4l2 ( Quick Time camera) \nCamera device: '%s'\nCtrl+C to quit\n", device_name);
    //This allow will allow to read the keyboard without blocking
    run_capture(device_name, capture_callback, NULL); //This blocks until calling capture_callback with each new image until it return 0
    return 0;
}
