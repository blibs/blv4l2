find_package(blc_core REQUIRED)

find_path(LOCAL_INCLUDE_DIR blv4l2.h PATH_SUFFIXES blv4l2)
find_library(LOCAL_LIBRARY blv4l2 )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(blv4l2 DEFAULT_MSG  LOCAL_LIBRARY LOCAL_INCLUDE_DIR)

mark_as_advanced(LOCAL_INCLUDE_DIR LOCAL_LIBRARY )

set(BL_INCLUDE_DIRS ${LOCAL_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${LOCAL_LIBRARY} ${BL_LIBRARIES} )
